/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author lenovo
 */
public class Duck extends Animal {

    private int numberOfWing;

    public Duck(String name, String color) {
        super(name, color, 2);
        System.out.println("Duck Created");
    }

    public void fly() {
        System.out.println("Duck: " + name + " Fly!!");
    }

    @Override
    public void walk() {
        super.walk();
        System.out.println("Duck: " + name + " walk with " + numberOfLeg + " leg.");
    }

    @Override
    public void speak() {
        super.speak();
        System.out.println("Duck: " + name + " Speak > Gab Gab");
    }
}
