/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author lenovo
 */
public class Animal {

    protected String name;
    protected int numberOfLeg = 0;
    protected String color;

    public Animal(String name, String color, int numberOfLeg) {
        System.out.println("--------------------------");
        System.out.println("Animal Created");
        this.name = name;
        this.color = color;
        this.numberOfLeg = numberOfLeg;
    }

    public void walk() {
        System.out.println("Animal Walk");
    }

    public void speak() {
        System.out.println("Animal Speak");
        System.out.println("Name: " + this.name + " Color: " + this.color
                + " Number Of Leg: " + this.numberOfLeg);
    }

    public String getName() {
        return name;
    }

    public int getNumberOfLeg() {
        return numberOfLeg;
    }

    public String getColor() {
        return color;
    }

}
