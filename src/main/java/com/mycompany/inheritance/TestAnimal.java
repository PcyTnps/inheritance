/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author lenovo
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();

        Dog dang = new Dog("Dang", "Black & White");
        dang.speak();
        dang.walk();

        Dog mome = new Dog("Mome", "Black & White");
        mome.speak();
        mome.walk();

        Dog bat = new Dog("Bat", "Black & White");
        bat.speak();
        bat.walk();

        Dog to = new Dog("To", "Brown");
        to.speak();
        to.walk();

        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();

        Duck som = new Duck("Som", "Orange");
        som.speak();
        som.walk();
        som.fly();

        Duck gabgab = new Duck("GabGab", "Black");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        
        System.out.println("Dang is Animal: " + (dang instanceof Animal));
        System.out.println("Dang is Dog: " + (dang instanceof Dog));
        System.out.println("Dang is Object: " + (dang instanceof Object));
        System.out.println("Mome is Animal: " + (mome instanceof Animal));
        System.out.println("Mome is Dog: " + (mome instanceof Dog));
        System.out.println("Mome is Object: " + (mome instanceof Object));
        System.out.println("To is Animal: " + (to instanceof Animal));
        System.out.println("To is Dog: " + (to instanceof Dog));
        System.out.println("To is Object: " + (to instanceof Object));
        System.out.println("Bat is Animal: " + (bat instanceof Animal));
        System.out.println("Bat is Dog: " + (bat instanceof Dog));
        System.out.println("Bat is Object: " + (bat instanceof Object));
        System.out.println("Zero is Animal: " + (zero instanceof Animal));
        System.out.println("Zero is Cat: " + (zero instanceof Cat));
        System.out.println("Zero is Object: " + (zero instanceof Object));
        System.out.println("Som is Animal: " + (som instanceof Animal));
        System.out.println("Som is Duck: " + (som instanceof Duck));
        System.out.println("Som is Object: " + (som instanceof Object));
        System.out.println("GabGab is Animal: " + (gabgab instanceof Animal));
        System.out.println("GabGab is Duck: " + (gabgab instanceof Duck));
        System.out.println("GabGab is Object: " + (gabgab instanceof Object));
        System.out.println("Animal is Dog: " + (animal instanceof Dog));
        System.out.println("Animal is Cat: " + (animal instanceof Cat));
        System.out.println("Animal is Duck: " + (animal instanceof Duck));
        System.out.println("Animal is Animal: " + (animal instanceof Animal));

        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = som;
        ani2 = zero;

        System.out.println("Ani1: som is Duck " + (ani1 instanceof Duck));

        Animal[] animals = {dang, bat, to, mome, zero, som, gabgab};
        for (int i = 0; i < animals.length; i++) {
            System.out.println("Animal Created");
            animals[i].walk();
            animals[i].speak();
            if (animals[i] instanceof Duck) {
                Duck duck = (Duck) animals[i];
                duck.fly();
            }
            System.out.println("---------------------");
        }
    }
}
